
package lab5part2;

/**
 *
 * @author Henry
 */
public class Student {
    private String rollNo;
    private String name;
    private String id;
    
    
    // getters and setters

    public String getRollNo() {
        return rollNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    }
 
       
    
    


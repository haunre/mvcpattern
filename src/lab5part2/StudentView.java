/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab5part2;

/**
 *
 * @author Henry
 */
public class StudentView {
    public void printStudentDetails(String name, String rollNo, String id){
        System.out.println("Student:\n Name:" + name + "\nRoll No:" + rollNo + "\nId:" + id);
    }
    
}

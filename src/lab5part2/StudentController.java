
package lab5part2;

/**
 *
 * @author Henry
 */
public class StudentController {
    private StudentView view;
    private Student model;

    public StudentController() {
    }

    public StudentController(StudentView view, Student model) {
        this.view = view;
        this.model = model;
    }

    public StudentView getView() {
        return view;
    }

    public void setView(StudentView view) {
        this.view = view;
    }

    public Student getModel() {
        return model;
    }

    public void setModel(Student model) {
        this.model = model;
    }
    public void updateView(){
        view.printStudentDetails(model.getName(), model.getRollNo(), model.getId());
    }
    
    
}
 